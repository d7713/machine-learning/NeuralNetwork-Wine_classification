# Wine grade classification (Neural Network)

### By: Andrew Wairegi

## Description
To determine the quality of a wine given the following features. This will allow wine
companies, to be able to determine the qualities of their wine without tasting. However, 
the wine quality changes as time goes on. As we the features change as time goes on, of what determines
the grade of a wine. This is the purpose of the notebook.

[Open notebook]

## Setup/installation instructions
1. Create a local folder on your computer
2. Set it up as an empty repository (using git init)
3. Clone this repository there (git clone https://...)
4. Upload the notebook to google drive
5. Open it
6. Upload the data file to google collab (file upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs.

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/NeuralNetwork-Wine_classification/-/blob/main/Wine%20classification%20(neural%20network).ipynb
